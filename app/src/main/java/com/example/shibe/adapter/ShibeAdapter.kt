package com.example.shibe.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shibe.databinding.ItemShibeBinding
import com.squareup.picasso.Picasso

class ShibeAdapter : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    private var shibes = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShibeViewHolder {
        return ShibeViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        holder.loadShibe(shibes[position])
    }

    override fun getItemCount(): Int {
        return shibes.size
    }

    fun addShibes(shibes: List<String>) {
        this.shibes = shibes.toMutableList()
        notifyDataSetChanged()
    }

    class ShibeViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibe(shibe: String) = with(binding) {
            Picasso.get().load(shibe).into(ivShibe)
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { ShibeViewHolder(it) }
        }
    }
}