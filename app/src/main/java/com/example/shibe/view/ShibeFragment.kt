package com.example.shibe.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.shibe.adapter.ShibeAdapter
import com.example.shibe.databinding.FragmentShibeBinding
import com.example.shibe.viewmodel.ShibeViewModel

class ShibeFragment : Fragment() {

    private var _binding: FragmentShibeBinding? = null
    private val binding get() = _binding!!
    private val shibeViewModel by viewModels<ShibeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var state = "linear"

        fun view() {
            with(binding.rvListShibe) {

                if (state == "linear") {
                    layoutManager =
                        LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)

                } else if (state == "grid") {
                    layoutManager =
                        GridLayoutManager(this.context, 2, GridLayoutManager.VERTICAL, false)
                } else {
                    layoutManager =
                        StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
                }

                adapter = ShibeAdapter().apply {
                    with(shibeViewModel) {
                        getShibes()
                        stateList.observe(viewLifecycleOwner) {
                            addShibes(it)
                        }
                    }
                }
            }
        }
        view()

        binding.btnLinear.setOnClickListener {
            state = "linear"
            view()
        }
        binding.btnGrid.setOnClickListener {
            state = "grid"
            view()
        }
        binding.btnStaggered.setOnClickListener {
            state = "stagger"
            view()
        }

    }

}