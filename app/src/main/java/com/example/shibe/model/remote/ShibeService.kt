package com.example.shibe.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET

interface ShibeService {
    companion object {
        private const val BASE_URL = "https://shibe.online"

        fun getInstance(): ShibeService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }
    }

    @GET("/api/shibes?count=100&urls=true&httpsUrls=true")
    suspend fun getShibes(): List<String>
}