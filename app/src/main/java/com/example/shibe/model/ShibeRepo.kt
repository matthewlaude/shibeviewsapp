package com.example.shibe.model

import com.example.shibe.model.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ShibeRepo {

    private val shibeService: ShibeService by lazy { ShibeService.getInstance() }

    suspend fun getShibes(): List<String> = withContext(Dispatchers.IO) {
        shibeService.getShibes()
    }
}