package com.example.shibe.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibe.model.ShibeRepo
import kotlinx.coroutines.launch

class ShibeViewModel : ViewModel() {
    private val repo by lazy { ShibeRepo }

    private val _stateList = MutableLiveData<List<String>>()
    val stateList: LiveData<List<String>> get() = _stateList

    fun getShibes() {
        viewModelScope.launch {
            val strings = repo.getShibes()
            _stateList.value = strings
        }
    }
}